﻿using System;
using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;

[RequireComponent(typeof(CanvasGroup))]
public class IPopup : MonoBehaviour
{

    public Button closeBtn;
    public CanvasGroup group;

    private static Tween m;
    private static Sequence s;

    private void Reset()
    {
        @group = GetComponent<CanvasGroup>();
    }

    public virtual void Start()
    {
        closeBtn.onClick.AddListener(OnClose);
    }

    public virtual void OnClose()
    {
        s.Join(m = @group.DOFade(0, 0.2f).OnComplete(() =>
        {
            this.Hide();
            UI.ins.popup.Hide();
        })).Join(UI.ins.popup.Fade());
        s.Restart();
    }

    public virtual void OnShow()
    {
        s?.Kill();
        m?.Kill();
        UI.ins.popup.OnShow();
        this.Show();
    }


}
