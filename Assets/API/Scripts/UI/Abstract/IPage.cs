﻿using System;
using UnityEngine;
using System.Collections;
using DG.Tweening;
[RequireComponent(typeof(CanvasGroup))]
public class IPage : MonoBehaviour
{

    public CanvasGroup group;
    public float timeFade = 0.2f;

    private void Reset()
    {
        @group = GetComponent<CanvasGroup>();
    }

    public virtual void OnShow()
    {
        OnStartShow();
        @group.DOFade(1, timeFade).OnComplete(() => { @group.blocksRaycasts = true; });
    }

    public virtual void OnExit()
    {
        OnStartExit();
        @group.blocksRaycasts = false;
        @group.DOFade(0, timeFade);
    }

    public virtual void OnStartShow()
    {

    }

    public virtual void OnStartExit()
    {

    }

}
