﻿using System;
using UnityEngine;
using System.Collections;

public class UI : MonoBehaviour
{
    public static UI ins;

    public UIMain main;
    public UIPopup popup;
    public UIGame game;

    private void Start()
    {
        InitOnStart();
    }

    private void InitOnStart()
    {
        popup.mission.OnRegisterEvent();
        popup.mission.CheckHasCompleteMission();

        popup.onlineReward.CheckHasRewardCanClaim();
    }

    // Nhận thông tin online reward
    public void OnlineRewardListener()
    {
        popup.onlineReward.ShowInfo();
        popup.onlineReward.ShowNotification();
    }

}
