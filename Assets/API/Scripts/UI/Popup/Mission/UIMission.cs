﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class UIMission : IPopup
{
    public List<UIMissionItem> items;

    public void OnRegisterEvent()
    {
        foreach (var missionType in Enum.GetValues(typeof(MissionType)).Cast<MissionType>())
        {
            this.RegisterMissionListener(missionType, (b) =>
            {
                var f = GameManager.ins.MissionInfo;
                if (CheckIsHasOnActiveMission(missionType, f))
                {
                    var index = f.FindIndex(s => s.missionType == missionType);
                    if (f[index].missionState == State.Unfinished)
                    {
                        f[index].missionProgress++;
                        if (f[index].missionProgress >= f[index].valueToCompleteMission)
                        {
                            f[index].missionState = State.Completed;
                            NotificationWhenHasMissionComplete();
                        }
                    }
                }
            });
        }
    }

    public override void OnShow()
    {
        base.OnShow();

        ShowInfo();

    }

    public void ShowInfo()
    {
        var f = GameManager.ins.MissionInfo;
        var c = 0;
        foreach (var item in items)
        {
            item.OnShow(f[c]);
            c++;
        }
    }

    public bool CheckIsHasOnActiveMission(MissionType missionType, List<PlayerMissionInfo> missions)
    {
        foreach (var info in missions)
        {
            if (info.missionType == missionType)
            {
                return true;
            }
        }

        return false;
    }

    /// <summary>
    ///  Gửi đến người chơi khi có một mission nào đấy hoàn thành. Có thể là show lên thông báo, biếu tượng....
    /// </summary>
    public void NotificationWhenHasMissionComplete()
    {

    }

    public void CheckHasCompleteMission()
    {
        foreach (var item in GameManager.ins.MissionInfo)
        {
            if (item.missionState == State.Completed)
            {
                NotificationWhenHasMissionComplete();
                return;
            }
        }
    }

}
