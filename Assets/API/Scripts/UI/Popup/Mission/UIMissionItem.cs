﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIMissionItem : MonoBehaviour
{
    // Khai báo các trường cần hiển thị như text mission name, mission description, button claim...

    public Button claimBtn;
    public Image claimIm;
    public GameObject claimedGO;

    private bool isCanClaim;

    private MissionInfo missionInfo;

    private void Start()
    {
        claimBtn.onClick.AddListener(OnClaimReward);
    }

    private void OnClaimReward()
    {
        if (isCanClaim)
        {
            // Reward
            UI.ins.popup.rewardPopup.OnShow(missionInfo.reward);

            // Luu data
            var infors = GameManager.ins.MissionInfo;
            var i = infors.FindIndex(s => s.missionType == missionInfo.missionType);
            infors[i].missionState = State.Claimed;
            GameManager.ins.MissionInfo = infors;

            // Update Information
            UI.ins.popup.mission.ShowInfo();
        }
    }

    public void OnShow(PlayerMissionInfo info)
    {
        missionInfo = GameManager.ins.missionData.missionData.Find(s => s.missionType == info.missionType);

        //Show cac thong tin lien quan


        // Show cac trang thai mission
        switch (info.missionState)
        {
            case State.Unfinished:

                claimBtn.Show();
                claimedGO.Hide();
                isCanClaim = false;
                claimIm.sprite = GameManager.ins.iconData.cantClaimBtnIm;

                break;
            case State.Completed:

                claimBtn.Show();
                claimedGO.Hide();
                isCanClaim = true;
                claimIm.sprite = GameManager.ins.iconData.claimBtnIm;

                break;
            case State.Claimed:

                claimBtn.Hide();
                claimedGO.Show();
                isCanClaim = false;

                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

    }

}
