﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UISetting : IPopup
{
    public Button soundBtn, musicBtn;
    public Image soundIm, musicIm;

    public override void Start()
    {
        base.Start();
        soundBtn.onClick.AddListener(ChangeSound);
        musicBtn.onClick.AddListener(ChangeMusic);
    }

    private void RateUs()
    {
    }

    private void ShowFb()
    {
    }

    private void ChangeMusic()
    {
        GameManager.ins.IsMusicOn = !GameManager.ins.IsMusicOn;
        musicIm.sprite = GameManager.ins.IsMusicOn ?GameManager.ins.iconData.soundOn : GameManager.ins.iconData.soundOff;
    }

    private void ChangeSound()
    {
        GameManager.ins.IsSoundOn = !GameManager.ins.IsSoundOn;
        soundIm.sprite = GameManager.ins.IsSoundOn ? GameManager.ins.iconData.soundOn : GameManager.ins.iconData.soundOff;
    }

    public override void OnShow()
    {
        base.OnShow();
        ShowInfo();
    }

    public void ShowInfo()
    {
        musicIm.sprite = GameManager.ins.IsMusicOn ? GameManager.ins.iconData.soundOn : GameManager.ins.iconData.soundOff;
        soundIm.sprite = GameManager.ins.IsSoundOn ? GameManager.ins.iconData.soundOn : GameManager.ins.iconData.soundOff;
    }

}
