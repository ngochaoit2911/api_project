﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class UI7Days : IPopup
{
    public List<UI7DaysItem> items;

    public override void OnShow()
    {
        base.OnShow();

        ShowInfo();

    }

    internal void ShowInfo()
    {
        var b = GameManager.ins.IsCanRewardOnDay();

        if (b)
        {
            for (int i = 0; i < items.Count; i++)
            {
                if (i < GameManager.ins.RewardDayCount)
                {
                    items[i].OnShow(State.Claimed);
                }else if (i == GameManager.ins.RewardDayCount)
                {
                    items[i].OnShow(State.Completed);
                }
                else
                {
                    items[i].OnShow(State.Unfinished);
                }
            }
        }
        else
        {
            for (int i = 0; i < items.Count; i++)
            {
                items[i].OnShow(i <= GameManager.ins.RewardDayCount ? State.Claimed : State.Unfinished);
            }
        }

    }

}
