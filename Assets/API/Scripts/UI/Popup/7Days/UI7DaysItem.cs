﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UI7DaysItem : MonoBehaviour
{
    public int index;
    private bool isCanClaim;

    public Button claimBtn;

    private void Start()
    {
        claimBtn.onClick.AddListener(OnClaimReward);
    }

    private void OnClaimReward()
    {
        if (isCanClaim)
        {
            // Reward
            UI.ins.popup.rewardPopup.OnShow(GameManager.ins.data7Days.rewardData[index]);

            // Luu Data
            GameManager.ins.RewardDayCount++;
            GameManager.ins.RewardDayTime = DateTime.Now;

            // UpdateInfor

            UI.ins.popup.ui7Days.ShowInfo();
        }
    }

    public void OnShow(State state)
    {
        switch (state)
        {
            case State.Unfinished:
                isCanClaim = false;
                break;
            case State.Completed:
                isCanClaim = true;
                break;
            case State.Claimed:
                isCanClaim = false;
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(state), state, null);
        }
    }
}
