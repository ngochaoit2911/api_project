﻿using System;
using UnityEngine;
using System.Collections;

public class UIRewardPopup : IPopup
{

    public void OnShow(Reward reward)
    {
        // Nên sắp xếp các loại reward theo thứ tự ưu tiên, xuất hiện nhiều
        switch (reward.type)
        {
            case RewardType.Coin:
                break;
            case RewardType.Gem:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

}
