﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class UIOnlineRewardItem : MonoBehaviour
{
    public Button claimBtn;
    private bool isCanClaim;
    private OnlineRewardInfo info;

    private void Start()
    {
        claimBtn.onClick.AddListener(OnClaimReward);
    }

    private void OnClaimReward()
    {
        if (isCanClaim)
        {
            // Reward
            UI.ins.popup.rewardPopup.OnShow(info.reward);

            // Luu thong tin
            var f = GameManager.ins.OnlineRewardStates;
            f[info.index] = State.Claimed;
            GameManager.ins.OnlineRewardStates = f;

            // UpdateInformation
            UI.ins.popup.onlineReward.ShowInfo();
        }
    }

    public void OnShow(OnlineRewardInfo info, State state)
    {
        this.info = info;
        switch (state)
        {
            case State.Unfinished:
                isCanClaim = false;
                break;
            case State.Completed:
                isCanClaim = true;
                break;
            case State.Claimed:
                isCanClaim = false;
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(state), state, null);
        }
    }
}
