﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UIOnlineReward : IPopup
{
    public List<UIOnlineRewardItem> items;

    public override void OnShow()
    {
        base.OnShow();

        ShowInfo();

    }

    public void ShowInfo()
    {
        var i = 0;
        var data = GameManager.ins.onlineRewardData.rewardData;
        foreach (var item in items)
        {
            item.OnShow(data[i], GameManager.ins.OnlineRewardStates[i]);
            i++;
        }
    }

    public void CheckHasRewardCanClaim()
    {
        foreach (var state in GameManager.ins.OnlineRewardStates)
        {
            if (state == State.Completed)
            {
                ShowNotification();
                return;
            }
        }
    }

    // Hiển thị thông báo cho người dùng bằng các animation, ui..
    public void ShowNotification()
    {

    }

}
