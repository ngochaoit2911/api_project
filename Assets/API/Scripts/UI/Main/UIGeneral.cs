﻿
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class UIGeneral : MonoBehaviour
{
    public Text coinTxt, gemTxt;

    private float temp1, temp2;
    public float time = 0.3f;

    public void UpdateCoin()
    {
        var x = GameManager.ins.PlayerCoin;
        DOVirtual.Float(temp1, x, time, value => { coinTxt.text = ((int)value).ToString(); })
            .OnComplete(() => { temp1 = x; });
    }

    public void UpdateGem()
    {
        var x = GameManager.ins.PlayerGem;
        DOVirtual.Float(temp2, x, time, value => { gemTxt.text = ((int)value).ToString(); })
            .OnComplete(() => { temp2 = x; });
    }
}

