﻿
 using DG.Tweening;
 using UnityEngine;
 using UnityEngine.UI;

 public class UIPopup : MonoBehaviour
 {
     public Image panel;

     public UIMission mission;
     public UIOnlineReward onlineReward;
     public UISetting setting;
     public UI7Days ui7Days;
     public UIRewardPopup rewardPopup;

     public Tween Fade()
     {
         return panel.DOFade(0, 0.2f);
     }

     public void OnShow()
     {
         this.Show();
         panel.DOFade(1, 0.1f);
     }
 }

