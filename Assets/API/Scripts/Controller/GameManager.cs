﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager ins;

    class Fixed
    {  
        // Khi muốn thay đổi data thì chỉ cần thay đổi string Isfirstplay là được
        public const string IsFirstPlayGame = "IsFirstPlay";
        public const string PlayerCoin = "PlayerCoin";
        public const string PlayerGem = "PlayerGem";
        public const string VideoCount = "VideoCount";
        public const string Sound = "Sound";
        public const string Music = "Music";
        public const string OnlineTime = "OnlineTime";
        public const string OnlineRewardState = "OnlineRewardState";
        public const string DayRewardTime = "DayRewardTime";
        public const string RewardDayCount = "RewardDayCount";
        public const string Mission = "Mission";
    }

    #region Field

    private int playerCoin;
    private int playerGem;
    private int videoCount;
    private bool isSoundOn;
    private bool isMusicOn;
    private int onlineTimeCount;
    private DateTime rewardDayTime;
    private int rewarDayCount;
    private List<PlayerMissionInfo> missionInfo;
    private List<State> onlineRewardStates;

    #endregion

    #region Property

    public List<State> OnlineRewardStates
    {
        get => onlineRewardStates;
        set
        {
            onlineRewardStates = value;
            EncryptHelper.SaveList(Fixed.OnlineRewardState, value);
        }
    }


    public List<PlayerMissionInfo> MissionInfo
    {
        get => missionInfo;
        set
        {
            missionInfo = value;
            EncryptHelper.SaveList(Fixed.Mission, value);
        }
    }

    public int RewardDayCount
    {
        get => rewarDayCount;
        set
        {
            rewarDayCount = value;
            EncryptHelper.SetInt(Fixed.RewardDayCount, value);
        }
    }

    public int OnlineTimeCount
    {
        get => onlineTimeCount;
        set
        {
            onlineTimeCount = value;
            EncryptHelper.SetInt(Fixed.OnlineTime, value);
        }
    }

    public DateTime RewardDayTime
    {
        get => rewardDayTime;
        set
        {
            rewardDayTime = value;
            EncryptHelper.SetString(Fixed.DayRewardTime, value.ToString());
        }
    }

    public bool IsMusicOn
    {
        get => isMusicOn;
        set
        {
            isMusicOn = value;
            EncryptHelper.SetInt(Fixed.Music, value ? 1 : 0);
        }
    }

    public bool IsSoundOn
    {
        get => isSoundOn;
        set
        {
            isSoundOn = value;
            EncryptHelper.SetInt(Fixed.Sound, value ? 1 : 0);
        }
    }

    public int PlayerCoin
    {
        get => playerCoin;
        set
        {
            playerCoin = value;
            EncryptHelper.SetInt(Fixed.PlayerCoin, value);
        }
    }

    public int PlayerGem
    {
        get => playerGem;
        set
        {
            playerGem = value;
            EncryptHelper.SetInt(Fixed.PlayerGem, value);
        }
    }

    public int VideoCount
    {
        get => videoCount;
        set
        {
            videoCount = value;
            EncryptHelper.SetInt(Fixed.VideoCount, value);
        }
    }

    #endregion

    #region Data

    public OnlineRewardData onlineRewardData;
    public MissionData missionData;
    public IconData iconData;
    public Data7Days data7Days;

    #endregion

    private void Awake()
    {
        ins = this;

        IsFirstPlay();
        
    }

    private void Start()
    {
        StartCountOnline();
    }

    private void IsFirstPlay()
    {
        if (!PlayerPrefs.HasKey(Fixed.IsFirstPlayGame))
        {
            // Khởi tạo dữ liệu: Ví dụ dữ liệu người dùng như coin, gem, dữ liệu nhiệm vụ, rank...

            //Coin
            PlayerCoin = 0;
            PlayerGem = 0;
            VideoCount = 0;
            IsSoundOn = true;
            IsMusicOn = true;
            OnlineTimeCount = 0;

            onlineRewardStates = new List<State>();
            foreach (var v in onlineRewardData.timeRewards)
            {
                onlineRewardStates.Add(State.Unfinished);
            }

            OnlineRewardStates = onlineRewardStates;

            RewardDayTime = DateTime.MinValue;
            RewardDayCount = 0;
            InitMission();

        }
        else
        {
            // Lấy ra các dữ liệu đã được khởi tạo trước đây
            playerCoin = EncryptHelper.GetInt(Fixed.PlayerCoin);
            playerGem = EncryptHelper.GetInt(Fixed.PlayerGem);
            videoCount = EncryptHelper.GetInt(Fixed.VideoCount);
            isSoundOn = EncryptHelper.GetInt(Fixed.Sound) == 1;
            isMusicOn = EncryptHelper.GetInt(Fixed.Music) == 1;
            onlineTimeCount = EncryptHelper.GetInt(Fixed.OnlineTime);
            rewardDayTime = GameHelper.ConvertStringToDateTime(EncryptHelper.GetString(Fixed.DayRewardTime));
            missionInfo = EncryptHelper.LoadList<PlayerMissionInfo>(Fixed.Mission);
            rewarDayCount = EncryptHelper.GetInt(Fixed.RewardDayCount);
        }
    }

    public bool IsCanRewardOnDay()
    {
        var date = DateTime.Now;
        var span = date - rewardDayTime;
        var day = span.TotalDays;
        return day > 1;
    }

    private Coroutine onlineCt;

    public void StartCountOnline()
    {
        if (onlineCt != null)
        {
            StopCoroutine(onlineCt);
        }

        onlineCt = StartCoroutine(IOnline());
    }

    IEnumerator IOnline()
    {
        var wt = new WaitForSeconds(1);
        var maxTime = onlineRewardData.timeRewards[onlineRewardData.timeRewards.Count - 1];
        while (true)
        {
            onlineTimeCount++;

            for (int i = 0; i < onlineRewardStates.Count; i++)
            {
                if (onlineTimeCount >= onlineRewardData.timeRewards[i] && onlineRewardStates[i] == State.Unfinished)
                {
                    onlineRewardStates[i] = State.Completed;
                    OnlineRewardStates = onlineRewardStates;

                    UI.ins.OnlineRewardListener();
                }
            }

            if (onlineTimeCount > maxTime)
            {
                yield break;
            }

            yield return wt;
        }
    }

    public void InitMission()
    {
        missionInfo = new List<PlayerMissionInfo>();

        for (int i = 0; i < missionData.missionCountOnInit; i++)
        {
            missionInfo.Add(new PlayerMissionInfo
            {
                missionName = missionData.missionData[i].missionName,
                missionType = missionData.missionData[i].missionType,
                missionProgress = 0,
                valueToCompleteMission = missionData.missionData[i].valueToComplete,
                missionState = State.Unfinished
            });
        }

        MissionInfo = missionInfo;
    }
        
    private void OnApplicationPause(bool pauseStatus)
    {
        OnlineTimeCount = onlineTimeCount;
    }
}
