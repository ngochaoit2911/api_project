﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CreateAssetMenu(menuName = "Data/Mission")]
public class MissionData : ScriptableObject
{
    public int missionCountOnInit = 3;

    public List<MissionInfo> missionData;
}

[System.Serializable]
public struct MissionInfo
{
    public string missionName;
    public string missionDescription;
    public MissionType missionType;
    public int valueToComplete;

    //Reward
    public Reward reward;
}

public enum MissionType
{
    // Đánh dấu các mission bằng ID của nó
    Mission1, Mission2, Mission3 //...
}