﻿
[System.Serializable]
public class PlayerMissionInfo
{
    public string missionName;
    public int missionProgress;
    public int valueToCompleteMission;
    public State missionState;
    public MissionType missionType;
}

public enum State
{
    Unfinished, Completed, Claimed 
}
