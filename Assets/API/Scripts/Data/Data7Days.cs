﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CreateAssetMenu(menuName = "Data/7Days")]
public class Data7Days : ScriptableObject
{
    public List<Reward> rewardData;
}
