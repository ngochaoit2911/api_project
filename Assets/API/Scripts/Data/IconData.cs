﻿using UnityEngine;
using UnityEditor;

[CreateAssetMenu(menuName = "Data/IconData")]
public class IconData : ScriptableObject
{
    public Sprite claimBtnIm, cantClaimBtnIm;
    public Sprite soundOn;
    public Sprite soundOff;
}