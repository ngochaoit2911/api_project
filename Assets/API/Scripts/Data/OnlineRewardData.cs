﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CreateAssetMenu(menuName = "Data/OnlineReward", fileName = "OnlineRewardData")]
public class OnlineRewardData : ScriptableObject
{
    public List<int> timeRewards;

    // Chỗ này có thể không cần data, Có thể switch-case theo thứ tự để nhận thưởng
    public List<OnlineRewardInfo> rewardData;
}

[System.Serializable]
public struct OnlineRewardInfo
{
    public int index;
    public Reward reward;
}

[System.Serializable]
public struct Reward
{
    public RewardType type;
    public int rewardValue;
    // Có thể có 1 số loại reward không quy ra được số, 
}

public enum RewardType
{
    Coin, Gem, //...
}