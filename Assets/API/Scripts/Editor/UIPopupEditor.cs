﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(UIPopup))]
public class UIPopupEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
    }
}